#include "../../lv_examples.h"
#if LV_USE_PNG && LV_USE_IMG && LV_BUILD_EXAMPLES

/**
 * Open a PNG image from a file and a variable
 */
void lv_example_png_1(void)
{
    LV_IMG_DECLARE(img_wink_png);
    lv_obj_t * img;


    img = lv_img_create(lv_scr_act());
    /* Assuming a File system is attached to letter 'A'
     * E.g. set LV_USE_FS_STDIO 'A' in lv_conf.h */
    lv_img_set_src(img, "A:/home/gec/lv_port_pc_vscode-master/1.png");
    lv_obj_set_pos(img, 0, 0);

    lv_obj_set_height(img, 50);
    lv_obj_set_width(img, 50);
    
    lv_img_set_zoom(img, 256);
    

    
    lv_obj_t *label = lv_label_create(lv_scr_act());
    lv_label_set_text(label, "liangzai");
    lv_obj_set_pos(label, 200, 100);
    
}

#endif
