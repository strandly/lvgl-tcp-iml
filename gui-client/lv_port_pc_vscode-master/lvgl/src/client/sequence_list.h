#ifndef SEQUENCE_LIST_H
#define SEQUENCE_LIST_H

/**
顺序表
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "public_define.h"

extern int dilatation;  // 扩容系数
extern int initia_size; // 初始大小

arr_list *sequence_create();                           // 顺序表创建
void sequence_dilatation(arr_list *list);              // 顺序表扩容,两倍
arr_list *sequence_insert(arr_list *list, void *data); // 顺序表插入,尾插
int sequence_del_pos(arr_list *list, int position);    // 删除指定位置的元素
int sequence_clear(arr_list *list);                    // 清空顺序表
void sequence_print_client(arr_list *list);            // 输出client顺序表

#endif // SEQUENCE_LIST_H
