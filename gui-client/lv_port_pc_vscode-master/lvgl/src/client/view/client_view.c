#include "client_view.h"

/**********************
 *  STATIC VARIABLES
 **********************/
static lv_obj_t *tv;
static lv_obj_t *kb;
static lv_obj_t *t2;                // 第二页
static lv_style_t style_text_muted; // 字体风格
static lv_style_t style_title;      // 标题风格
static lv_style_t style_outline;    // 边框风格

/**********************
 *  VARIABLES
 **********************/
lv_obj_t *panel4 = NULL;  // 当前的对话界面
lv_obj_t *group_view_box; // 群聊对话界面
static const lv_font_t *font_large;
client_data *current_dialog = NULL; // 当前对话的客户端
lv_obj_t *list1;                    // 左侧列表
arr_list *list_btn_list = NULL;     // 左侧列表的按钮顺序表
int img_name = 1;                   // 表示第几张图片
char img_name_arr[10][10] = {"1.png", "2.png", "3.png", "4.png", "5.png",
                             "6.png", "7.png", "8.png", "9.png", "10.png"}; // 头像图片名称
int i = 0;

// 总设置
void client_vew(void)
{
    style_init();

    // test_list_data();
    // test_dialog_data();

    tv = lv_tabview_create(lv_scr_act(), LV_DIR_TOP, 50);
    lv_obj_t *t1 = lv_tabview_add_tab(tv, "Group");
    t2 = lv_tabview_add_tab(tv, "Single");

    group_page(t1);

    single_page(t2);
}

// 风格初始化
void style_init()
{
    font_large = &lv_font_montserrat_24; // 设置字体描述符

    lv_style_init(&style_text_muted);
    lv_style_set_text_opa(&style_text_muted, LV_OPA_50);

    lv_style_init(&style_title);
    lv_style_set_text_font(&style_title, font_large);

    lv_style_init(&style_outline);
    /*Set a background color and a radius*/
    // lv_style_set_radius(&style_outline, 5);
    lv_style_set_bg_opa(&style_outline, LV_OPA_COVER);
    lv_style_set_bg_color(&style_outline, lv_palette_lighten(LV_PALETTE_GREY, 1));

    /*Add outline*/
    lv_style_set_outline_width(&style_outline, 2); // 外边框距
    lv_style_set_outline_color(&style_outline, lv_palette_main(LV_PALETTE_BLUE));
    // lv_style_set_outline_pad(&style_outline, 8);  // 内边框距
}

// 群聊界面
static void group_page(lv_obj_t *parent)
{
    /*Create a keyboard*/
    lv_obj_t *kb = lv_keyboard_create(lv_scr_act());
    lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    /*Create message view box*/
    group_view_box = lv_obj_create(parent);
    lv_obj_remove_style_all(group_view_box); // delete style
    lv_obj_set_pos(group_view_box, 0, 0);
    lv_obj_set_size(group_view_box, 760, 280);
    // lv_obj_add_style(group_view_box, &style_outline, 0);

    // 生成群聊对话消息列表
    group_create_message_item(group_view_box);

    /*Create the input box*/
    lv_obj_t *panel2 = lv_obj_create(parent);
    lv_obj_remove_style_all(panel2); // delete style
    lv_obj_set_pos(panel2, 0, 300);
    lv_obj_set_size(panel2, 760, 80);
    // lv_obj_add_style(panel2, &style_outline, 0);

    /*Create the input*/
    lv_obj_t *input_box = lv_textarea_create(panel2);
    lv_textarea_set_one_line(input_box, true);
    lv_textarea_set_placeholder_text(input_box, "Input");
    lv_obj_set_width(input_box, lv_pct(100));  // 文本框宽度铺满
    lv_obj_set_height(input_box, lv_pct(100)); // 文本框高度铺满
    lv_obj_add_event_cb(input_box, keyboard_event_cb, LV_EVENT_ALL, kb);

    /*Create send btn*/
    lv_obj_t *send_btn = lv_btn_create(panel2);
    lv_obj_align(send_btn, LV_ALIGN_BOTTOM_RIGHT, 0, 0); // 设为右下角
    lv_obj_set_height(send_btn, LV_SIZE_CONTENT);
    lv_obj_add_event_cb(send_btn, group_send_msg_event_cb, LV_EVENT_ALL, input_box); // 点击发送时将文本框的内容发送

    // 设置按钮名称
    lv_obj_t *btn_label = lv_label_create(send_btn);
    lv_label_set_text(btn_label, "Send");
}

// 单聊界面
static void single_page(lv_obj_t *parent)
{

    /*Create a keyboard*/
    lv_obj_t *kb = lv_keyboard_create(lv_scr_act());
    lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    /*Create the input box*/
    lv_obj_t *panel2 = lv_obj_create(parent);
    lv_obj_remove_style_all(panel2); // delete style
    lv_obj_set_pos(panel2, 220, 310);
    lv_obj_set_size(panel2, 540, 80);
    // lv_obj_add_style(panel2, &style_outline, 0);

    /*Create the input*/
    lv_obj_t *input_box = lv_textarea_create(panel2);
    lv_textarea_set_one_line(input_box, true);
    lv_textarea_set_placeholder_text(input_box, "Input");
    lv_obj_set_width(input_box, lv_pct(100)); // 文本框宽度铺满
    lv_obj_set_height(input_box, lv_pct(100));
    lv_obj_add_event_cb(input_box, keyboard_event_cb, LV_EVENT_ALL, kb);

    /*Create send btn*/
    lv_obj_t *send_btn = lv_btn_create(panel2);
    lv_obj_align(send_btn, LV_ALIGN_BOTTOM_RIGHT, 0, 0); // 设为右下角
    lv_obj_set_height(send_btn, LV_SIZE_CONTENT);
    lv_obj_add_event_cb(send_btn, send_msg_event_cb, LV_EVENT_ALL, input_box); // 点击发送时将文本框的内容发送

    // 设置按钮名称
    lv_obj_t *btn_label = lv_label_create(send_btn);
    lv_label_set_text(btn_label, "Send");

    /*Create client list box*/
    lv_obj_t *panel3 = lv_obj_create(parent);
    lv_obj_remove_style_all(panel3); // delete style
    lv_obj_set_pos(panel3, 0, 0);
    lv_obj_set_size(panel3, 200, 390);
    // lv_obj_add_style(panel3, &style_outline, 0);

    /*Create client list*/
    list1 = lv_list_create(panel3);
    lv_obj_set_width(list1, lv_pct(100));
    lv_obj_set_height(list1, lv_pct(100));
    lv_obj_center(list1);

    /*Add buttons to the list*/
    create_list_option(list1);
}

// 生成列表选项
void create_list_option(lv_obj_t *list)
{
    client_single_setout();                  // 从服务端获取客户端数据
    sequence_print_client(client_data_list); // 打印这些数据
    if (list_btn_list == NULL)
    {
        list_btn_list = sequence_create();
    }
    else
    {
        // 清空,防止重复生成多个相同的标签
        for (int i = 0; i < list_btn_list->size; i++)
        {
            lv_obj_del((lv_obj_t *)list_btn_list->data[i]);
        }
        sequence_clear(list_btn_list);
    }

    lv_obj_t *btn;
    for (int i = 0; i < client_data_list->size; i++)
    {
        // 不显示自己的选项
        if (strcmp(((client_data *)client_data_list->data[i])->name, current_client.name) == 0)
        {
            continue;
        }
        btn = lv_list_add_btn(list, NULL, ((client_data *)client_data_list->data[i])->name);
        lv_obj_add_event_cb(btn, list_event_cb, LV_EVENT_ALL, (client_data *)client_data_list->data[i]);
        sequence_insert(list_btn_list, btn);
    }
}

// 键盘
static void keyboard_event_cb(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t *ta = lv_event_get_target(e);
    lv_obj_t *kb = lv_event_get_user_data(e);
    if (code == LV_EVENT_FOCUSED)
    {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD)
        {
            lv_keyboard_set_textarea(kb, ta);
            lv_obj_set_style_max_height(kb, LV_HOR_RES * 2 / 3, 0);
            lv_obj_update_layout(tv); /*Be sure the sizes are recalculated*/
            lv_obj_set_height(tv, LV_VER_RES - lv_obj_get_height(kb));
            lv_obj_clear_flag(kb, LV_OBJ_FLAG_HIDDEN);
            lv_obj_scroll_to_view_recursive(ta, LV_ANIM_OFF);
        }
    }
    else if (code == LV_EVENT_DEFOCUSED)
    {
        lv_keyboard_set_textarea(kb, NULL);
        lv_obj_set_height(tv, LV_VER_RES);
        lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);
        lv_indev_reset(NULL, ta);
    }
    else if (code == LV_EVENT_READY || code == LV_EVENT_CANCEL)
    {
        lv_obj_set_height(tv, LV_VER_RES);
        lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);
        lv_obj_clear_state(ta, LV_STATE_FOCUSED);
        lv_indev_reset(NULL, ta); /*To forget the last clicked object to make it focusable again*/
    }
}

// 列表点击事件
static void list_event_cb(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);
    if (code == LV_EVENT_CLICKED)
    {
        current_dialog = lv_event_get_user_data(e); // 获取传过来的客户端数据,确定当前对话的客户端
        LV_LOG_USER("list_event_cb name: %s ,fd: %d", current_dialog->name, current_dialog->fd);
        create_msg_view(current_dialog->name);
    }
}

// 群聊发送按钮点击事件
void group_send_msg_event_cb(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t *obj = lv_event_get_target(e);
    lv_obj_t *input_box = lv_event_get_user_data(e); // 获取传过来的文本框地址
    if (code == LV_EVENT_CLICKED)
    {
        char *send_text = lv_textarea_get_text(input_box);
        LV_LOG_USER("group_send_msg_event_cb msg:%s", send_text);
        // 发送数据
        client_group_send(send_text);

        // 显示发送的消息
        dialog_msg *new_dm = malloc(sizeof(dialog_msg));
        strcpy(new_dm->sender, current_client.name);
        strcpy(new_dm->text, send_text);
        strcpy(new_dm->send_head_portrait_name, current_client.send_head_portrait_name);
        sequence_insert(group_dialog_list, new_dm);
        group_insert_message_item(group_dialog_list->size - 1);
        show_group_dialog_list();
        // 清空文本框
        lv_textarea_set_cursor_pos(input_box, 0); // 光标移到开头
        while (strlen(lv_textarea_get_text(input_box)) != 0)
        {
            lv_textarea_del_char_forward(input_box);
        }
    }
}

// 发送按钮点击事件
static void send_msg_event_cb(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t *obj = lv_event_get_target(e);
    lv_obj_t *input_box = lv_event_get_user_data(e); // 获取传过来的文本框地址
    if (code == LV_EVENT_CLICKED)
    {
        char *send_text = lv_textarea_get_text(input_box);
        LV_LOG_USER("send_msg_event_cb name:%s,msg:%s", current_dialog->name, send_text);
        // 发送数据
        client_single_send(send_text, current_dialog->fd);
        // 显示发送的消息
        dialog_msg *new_dm = malloc(sizeof(dialog_msg));
        strcpy(new_dm->sender, current_client.name);
        strcpy(new_dm->send_head_portrait_name, current_client.send_head_portrait_name);
        strcpy(new_dm->receiver, current_dialog->name);
        strcpy(new_dm->text, send_text);
        sequence_insert(current_dialog->dm_list, new_dm);
        insert_message_item(current_dialog->dm_list->size - 1);
        // 清空文本框
        lv_textarea_set_cursor_pos(input_box, 0); // 光标移到开头
        while (strlen(lv_textarea_get_text(input_box)) != 0)
        {
            lv_textarea_del_char_forward(input_box);
        }
    }
}

// 模拟列表数据
void test_list_data()
{
    client_data_list = sequence_create();
    for (int i = 0; i < 10; i++)
    {
        char name[20];
        sprintf(name, "test%d", i);
        client_data *new_client = malloc(sizeof(client_data));
        memset(new_client, 0, sizeof(client_data));
        new_client->fd = i;
        strcpy(new_client->name, name);
        sequence_insert(client_data_list, new_client);
    }
}

// 模拟对话消息数据
void test_dialog_data()
{
    ((client_data *)client_data_list->data[0])->dm_list = sequence_create();
    dialog_msg *new_dm = malloc(sizeof(dialog_msg));
    strcpy(new_dm->sender, "test1");
    strcpy(new_dm->receiver, "test0");
    strcpy(new_dm->text, "hello");
    sequence_insert(((client_data *)client_data_list->data[0])->dm_list, new_dm);

    new_dm = malloc(sizeof(dialog_msg));
    strcpy(new_dm->sender, "test0");
    strcpy(new_dm->receiver, "test1");
    strcpy(new_dm->text, "world");
    sequence_insert(((client_data *)client_data_list->data[0])->dm_list, new_dm);

    new_dm = malloc(sizeof(dialog_msg));
    strcpy(new_dm->sender, "test1");
    strcpy(new_dm->receiver, "test0");
    strcpy(new_dm->text, "lili");
    sequence_insert(((client_data *)client_data_list->data[0])->dm_list, new_dm);

    for (int i = 0; i < ((client_data *)client_data_list->data[0])->dm_list->size; i++)
    {
        LV_LOG_USER("client_data:%s,receiver:%s,sender:%s,text:%s",
                    ((client_data *)client_data_list->data[0])->name,
                    ((dialog_msg *)((client_data *)client_data_list->data[0])->dm_list->data[i])->receiver,
                    ((dialog_msg *)((client_data *)client_data_list->data[0])->dm_list->data[i])->sender,
                    ((dialog_msg *)((client_data *)client_data_list->data[0])->dm_list->data[i])->text);
    }
}

// 生成群聊对话消息列表
void group_create_message_item(lv_obj_t *view_box)
{
    if (group_dialog_list == NULL)
    {
        group_dialog_list = sequence_create();
    }

    static lv_coord_t col_dsc[] = {152, 152, 152, 152, 152, LV_GRID_TEMPLATE_LAST};
    static lv_coord_t row_dsc[] = {20, 60, LV_GRID_TEMPLATE_LAST};

    for (int i = 0; i < group_dialog_list->size; i++)
    {
        lv_obj_t *cont = lv_obj_create(view_box);
        lv_obj_remove_style_all(cont);
        lv_obj_set_pos(cont, 0, 80 * i);
        lv_obj_set_width(cont, 760); // 宽度铺满
        lv_obj_set_height(cont, 80);
        lv_obj_add_style(cont, &style_outline, 0);
        lv_obj_set_grid_dsc_array(cont, col_dsc, row_dsc);

        lv_obj_t *label1, *label2, *img;
        char img_path[100]; // 图片路径
        memset(img_path, 0, sizeof(img_path));
        label1 = lv_label_create(cont);
        label2 = lv_label_create(cont);
        img = lv_img_create(cont);
        lv_obj_set_height(img, 50);
        lv_obj_set_width(img, 50);
        lv_img_set_zoom(img, 256);

        // 发送方是自己，则名字放在右边
        if (strcmp(current_client.name, ((dialog_msg *)group_dialog_list->data[i])->sender) == 0)
        {
            // 发送方名称
            lv_label_set_text(label1, ((dialog_msg *)group_dialog_list->data[i])->sender);
            lv_obj_add_style(label1, &style_text_muted, 0);
            lv_obj_set_grid_cell(label1, LV_GRID_ALIGN_END, 4, 1, LV_GRID_ALIGN_END, 0, 1);
            // 头像
            strcat(img_path, img_dir);
            strcat(img_path, ((dialog_msg *)group_dialog_list->data[i])->send_head_portrait_name);
            lv_img_set_src(img, img_path);
            lv_obj_set_grid_cell(img, LV_GRID_ALIGN_END, 4, 1, LV_GRID_ALIGN_END, 1, 1);
            // 消息内容
            lv_label_set_text(label2, ((dialog_msg *)group_dialog_list->data[i])->text);
            lv_obj_set_grid_cell(label2, LV_GRID_ALIGN_END, 4, 2, LV_GRID_ALIGN_END, 1, 1);
        }
        else
        {
            // 发送方名称
            lv_label_set_text(label1, ((dialog_msg *)group_dialog_list->data[i])->sender);
            lv_obj_add_style(label1, &style_text_muted, 0);
            lv_obj_set_grid_cell(label1, LV_GRID_ALIGN_START, 0, 1, LV_GRID_ALIGN_START, 0, 1);
            // 头像
            strcat(img_path, img_dir);
            strcat(img_path, ((dialog_msg *)group_dialog_list->data[i])->send_head_portrait_name);
            lv_img_set_src(img, img_path);
            lv_obj_set_grid_cell(img, LV_GRID_ALIGN_START, 0, 1, LV_GRID_ALIGN_START, 1, 1);
            // 消息内容
            lv_label_set_text(label2, ((dialog_msg *)group_dialog_list->data[i])->text);
            lv_obj_set_grid_cell(label2, LV_GRID_ALIGN_START, 1, 2, LV_GRID_ALIGN_START, 1, 1);
        }
    }
}

// 根据点击的列表事件生成对话框
void create_msg_view(char *client_name)
{
    /*Create message view box*/
    if (panel4 != NULL)
    {
        // 先清空画面
        lv_obj_del(panel4);
    }
    panel4 = lv_obj_create(t2);
    lv_obj_remove_style_all(panel4); // delete style
    lv_obj_set_pos(panel4, 220, 0);
    lv_obj_set_size(panel4, 540, 280);
    // lv_obj_add_style(panel4, &style_outline, 0);

    /*Create title view*/
    lv_obj_t *title = lv_label_create(panel4);                                         // 创建消息框标题
    lv_obj_set_grid_cell(title, LV_GRID_ALIGN_START, 0, 1, LV_GRID_ALIGN_START, 0, 1); // 设置字体样式
    lv_label_set_text(title, client_name);
    lv_obj_add_style(title, &style_title, 0);

    /*Create message item*/
    create_message_item(panel4, current_dialog);
}

// 生成对话消息列表
lv_obj_t *create_message_item(lv_obj_t *parent, client_data *current_dialog)
{
    if (current_dialog->dm_list == NULL)
    {
        current_dialog->dm_list = sequence_create();
    }

    static lv_coord_t col_dsc[] = {108, 108, 108, 108, 108, LV_GRID_TEMPLATE_LAST};
    static lv_coord_t row_dsc[] = {20, 60, LV_GRID_TEMPLATE_LAST};

    for (int i = 0; i < current_dialog->dm_list->size; i++)
    {
        lv_obj_t *cont = lv_obj_create(parent);
        lv_obj_remove_style_all(cont);
        lv_obj_set_pos(cont, 0, 80 + 80 * i);
        lv_obj_set_width(cont, 540); // 宽度铺满
        lv_obj_set_height(cont, 80);
        // lv_obj_add_style(cont, &style_outline, 0);
        lv_obj_set_grid_dsc_array(cont, col_dsc, row_dsc);

        lv_obj_t *label1, *label2, *img;
        char img_path[100]; // 图片路径
        memset(img_path, 0, sizeof(img_path));
        label1 = lv_label_create(cont);
        label2 = lv_label_create(cont);
        img = lv_img_create(cont);
        lv_obj_set_height(img, 50);
        lv_obj_set_width(img, 50);
        lv_img_set_zoom(img, 256);

        // 发送方是当前对话的客户端，则名字放在左边
        if (strcmp(current_dialog->name, ((dialog_msg *)current_dialog->dm_list->data[i])->sender) == 0)
        {
            // 发送方名称
            lv_label_set_text(label1, ((dialog_msg *)current_dialog->dm_list->data[i])->sender);
            lv_obj_add_style(label1, &style_text_muted, 0);
            lv_obj_set_grid_cell(label1, LV_GRID_ALIGN_START, 0, 1, LV_GRID_ALIGN_START, 0, 1);
            // 头像
            strcat(img_path, img_dir);
            strcat(img_path, ((dialog_msg *)current_dialog->dm_list->data[i])->send_head_portrait_name);
            lv_img_set_src(img, img_path);
            lv_obj_set_grid_cell(img, LV_GRID_ALIGN_START, 0, 1, LV_GRID_ALIGN_START, 1, 1);
            // 消息内容
            lv_label_set_text(label2, ((dialog_msg *)current_dialog->dm_list->data[i])->text);
            lv_obj_set_grid_cell(label2, LV_GRID_ALIGN_START, 1, 2, LV_GRID_ALIGN_START, 1, 1);
        }
        else
        {
            // 发送方名称
            lv_label_set_text(label1, ((dialog_msg *)current_dialog->dm_list->data[i])->sender);
            lv_obj_add_style(label1, &style_text_muted, 0);
            lv_obj_set_grid_cell(label1, LV_GRID_ALIGN_END, 4, 1, LV_GRID_ALIGN_END, 0, 1);
            // 头像
            strcat(img_path, img_dir);
            strcat(img_path, ((dialog_msg *)current_dialog->dm_list->data[i])->send_head_portrait_name);
            lv_img_set_src(img, img_path);
            lv_obj_set_grid_cell(img, LV_GRID_ALIGN_END, 4, 1, LV_GRID_ALIGN_END, 1, 1);
            // 消息内容
            lv_label_set_text(label2, ((dialog_msg *)current_dialog->dm_list->data[i])->text);
            lv_obj_set_grid_cell(label2, LV_GRID_ALIGN_START, 4, 2, LV_GRID_ALIGN_START, 1, 1);
        }
    }
    return NULL;
}

// 向对话消息列表中插入一条数据,pos为current_dialog中的第几个位置的数据
void group_insert_message_item(int pos)
{
    static lv_coord_t col_dsc[] = {152, 152, 152, 152, 152, LV_GRID_TEMPLATE_LAST};
    static lv_coord_t row_dsc[] = {20, 60, LV_GRID_TEMPLATE_LAST};

    lv_obj_t *cont = lv_obj_create(group_view_box);
    lv_obj_remove_style_all(cont);
    lv_obj_set_pos(cont, 0, 80 * pos);
    lv_obj_set_width(cont, 760); // 宽度铺满
    lv_obj_set_height(cont, 80);
    // lv_obj_add_style(cont, &style_outline, 0);
    lv_obj_set_grid_dsc_array(cont, col_dsc, row_dsc);

    lv_obj_t *label1, *label2, *img;
    char img_path[100]; // 图片路径
    memset(img_path, 0, sizeof(img_path));
    label1 = lv_label_create(cont);
    label2 = lv_label_create(cont);
    img = lv_img_create(cont);
    lv_obj_set_height(img, 50);
    lv_obj_set_width(img, 50);
    lv_img_set_zoom(img, 256);

    // 发送方是自己，则名字放在右边
    if (strcmp(current_client.name, ((dialog_msg *)group_dialog_list->data[pos])->sender) == 0)
    {
        // 发送方名称
        lv_label_set_text(label1, ((dialog_msg *)group_dialog_list->data[pos])->sender);
        lv_obj_add_style(label1, &style_text_muted, 0);
        lv_obj_set_grid_cell(label1, LV_GRID_ALIGN_END, 4, 1, LV_GRID_ALIGN_END, 0, 1);
        // 头像
        strcat(img_path, img_dir);
        strcat(img_path, ((dialog_msg *)group_dialog_list->data[pos])->send_head_portrait_name);
        lv_img_set_src(img, img_path);
        lv_obj_set_grid_cell(img, LV_GRID_ALIGN_END, 4, 1, LV_GRID_ALIGN_END, 1, 1);
        // 消息内容
        lv_label_set_text(label2, ((dialog_msg *)group_dialog_list->data[pos])->text);
        lv_obj_set_grid_cell(label2, LV_GRID_ALIGN_START, 4, 2, LV_GRID_ALIGN_START, 1, 1);
    }
    else
    {
        // 发送方名称
        lv_label_set_text(label1, ((dialog_msg *)group_dialog_list->data[pos])->sender);
        lv_obj_add_style(label1, &style_text_muted, 0);
        lv_obj_set_grid_cell(label1, LV_GRID_ALIGN_START, 0, 1, LV_GRID_ALIGN_START, 0, 1);
        // 头像
        strcat(img_path, img_dir);
        strcat(img_path, ((dialog_msg *)group_dialog_list->data[pos])->send_head_portrait_name);
        lv_img_set_src(img, img_path);
        lv_obj_set_grid_cell(img, LV_GRID_ALIGN_START, 0, 1, LV_GRID_ALIGN_START, 1, 1);
        // 消息内容
        lv_label_set_text(label2, ((dialog_msg *)group_dialog_list->data[pos])->text);
        lv_obj_set_grid_cell(label2, LV_GRID_ALIGN_START, 1, 2, LV_GRID_ALIGN_START, 1, 1);
    }
}

// 向对话消息列表中插入一条数据,pos为current_dialog中的第几个位置的数据
void insert_message_item(int pos)
{
    static lv_coord_t col_dsc[] = {108, 108, 108, 108, 108, LV_GRID_TEMPLATE_LAST};
    static lv_coord_t row_dsc[] = {20, 60, LV_GRID_TEMPLATE_LAST};

    lv_obj_t *cont = lv_obj_create(panel4);
    lv_obj_remove_style_all(cont);
    lv_obj_set_pos(cont, 0, 80 + 80 * pos);
    lv_obj_set_width(cont, 540); // 宽度铺满
    lv_obj_set_height(cont, 80);
    // lv_obj_add_style(cont, &style_outline, 0);
    lv_obj_set_grid_dsc_array(cont, col_dsc, row_dsc);

    lv_obj_t *label1, *label2, *img;
    char img_path[100]; // 图片路径
    memset(img_path, 0, sizeof(img_path));
    label1 = lv_label_create(cont);
    label2 = lv_label_create(cont);
    img = lv_img_create(cont);
    lv_obj_set_height(img, 50);
    lv_obj_set_width(img, 50);
    lv_img_set_zoom(img, 256);

    // 发送方是当前对话的客户端，则名字放在左边
    if (strcmp(current_dialog->name, ((dialog_msg *)current_dialog->dm_list->data[pos])->sender) == 0)
    {
        // 发送方名称
        lv_label_set_text(label1, ((dialog_msg *)current_dialog->dm_list->data[pos])->sender);
        lv_obj_add_style(label1, &style_text_muted, 0);
        lv_obj_set_grid_cell(label1, LV_GRID_ALIGN_START, 0, 1, LV_GRID_ALIGN_START, 0, 1);

        // 头像
        strcat(img_path, img_dir);
        strcat(img_path, ((dialog_msg *)current_dialog->dm_list->data[pos])->send_head_portrait_name);
        lv_img_set_src(img, img_path);
        lv_obj_set_grid_cell(img, LV_GRID_ALIGN_START, 0, 1, LV_GRID_ALIGN_START, 1, 1);

        // 消息内容
        lv_label_set_text(label2, ((dialog_msg *)current_dialog->dm_list->data[pos])->text);
        lv_obj_set_grid_cell(label2, LV_GRID_ALIGN_START, 1, 2, LV_GRID_ALIGN_START, 1, 1);
    }
    else
    {
        // 发送方名称
        lv_label_set_text(label1, ((dialog_msg *)current_dialog->dm_list->data[pos])->sender);
        lv_obj_add_style(label1, &style_text_muted, 0);
        lv_obj_set_grid_cell(label1, LV_GRID_ALIGN_END, 4, 1, LV_GRID_ALIGN_END, 0, 1);
        // 头像
        strcat(img_path, img_dir);
        strcat(img_path, ((dialog_msg *)current_dialog->dm_list->data[pos])->send_head_portrait_name);
        lv_img_set_src(img, img_path);
        lv_obj_set_grid_cell(img, LV_GRID_ALIGN_END, 4, 1, LV_GRID_ALIGN_END, 1, 1);
        // 消息内容
        lv_label_set_text(label2, ((dialog_msg *)current_dialog->dm_list->data[pos])->text);
        lv_obj_set_grid_cell(label2, LV_GRID_ALIGN_START, 3, 2, LV_GRID_ALIGN_START, 1, 1);
    }
}

// 打印group_dialog_list
void show_group_dialog_list()
{
    printf("================group_dialog_list================\n");
    for (int i = 0; i < group_dialog_list->size; i++)
    {
        printf("sender=%s,text=%s\n", ((dialog_msg *)group_dialog_list->data[i])->sender,
               ((dialog_msg *)group_dialog_list->data[i])->text);
    }
    printf("================group_dialog_list================\n");
}