/**
 * @file client_view.h
 *
 */

#ifndef CLIENT_VIEW_H
#define CLIENT_VIEW_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../lvgl.h"
#include "../public_define.h"
#include "../sequence_list.h"
#include "../tcp_client.h"

#define img_dir "A:/home/lwl/project/project3/lv_port_pc_vscode-master/picture/" // 图片目录路径

    extern client_info current_client;  // 当前客户端数据
    extern arr_list *client_data_list;  // 客户端数据顺序表
    extern arr_list *group_dialog_list; // 群聊对话消息顺序表
    extern client_data *current_dialog; // 当前对话的客户端
    extern lv_obj_t *panel4;            // 当前的对话界面
    extern lv_obj_t *list1;             // 左侧列表
    extern int img_name;                // 表示第几张图片
    extern char img_name_arr[10][10];   // 头像图片名称

    // 总设置
    extern void client_vew(void);
    // 风格初始化
    extern void style_init();
    // 群聊
    static void group_page(lv_obj_t *parent);
    // 单聊
    static void single_page(lv_obj_t *parent);
    // 键盘
    static void keyboard_event_cb(lv_event_t *e);
    // 生成列表选项
    void create_list_option(lv_obj_t *list);
    // 列表点击事件
    static void list_event_cb(lv_event_t *e);
    // 群聊发送按钮点击事件
    static void group_send_msg_event_cb(lv_event_t *e);
    // 单聊发送按钮点击事件
    static void send_msg_event_cb(lv_event_t *e);
    // 模拟客户端列表数据
    extern void test_list_data();
    // 模拟对话消息数据
    extern void test_dialog_data();
    // 生成群聊对话消息列表
    extern void group_create_message_item(lv_obj_t *view_box);
    // 根据点击的列表事件生成对话框
    extern void create_msg_view(char *client_name);
    // 生成对话列表
    extern lv_obj_t *create_message_item(lv_obj_t *parent, client_data *current_dialog);
    // 向群聊对话消息列表中插入一条数据,pos为group_dialog_list中的第几个位置的数据
    extern void group_insert_message_item(int pos);
    // 向对话消息列表中插入一条数据,pos为current_dialog中的第几个位置的数据
    extern void insert_message_item(int pos);
    // 打印group_dialog_list
    extern void show_group_dialog_list();

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif /*CLIENT_VIEW_H*/
