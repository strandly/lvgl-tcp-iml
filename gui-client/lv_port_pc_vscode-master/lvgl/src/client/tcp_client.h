/**
 * @file tcp_client.h
 *
 */

#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <pthread.h>
#include "public_define.h"
#include "view/client_view.h"

#include <sys/time.h>
#include <time.h>

#define SERVER_PORT 8000            // 服务器端口
#define SERVER_IP "139.224.247.174" // 服务端公网IP

    extern client_info current_client;  // 当前客户端数据
    extern client_data *current_dialog; // 当前对话的客户端
    extern arr_list *client_data_list;  // 客户端数据顺序表
    extern arr_list *group_dialog_list; // 群聊对话消息顺序表
    extern int flag;                    // 用于控制两个线程
    extern int read_flag;               // 用于控制读线程是否睡眠,1睡眠
    extern pthread_mutex_t mutex;       // 线程互斥锁
    extern pthread_cond_t cond;         // 线程条件变量
    extern lv_obj_t *list1;             // 左侧列表
    extern char img_name_arr[10][10];   // 头像图片名称

    // 总调用
    extern int tcp_client();
    // socket初始化
    extern int init_socket();
    // 客户端接收数据
    extern void *client_recv_action(void *args);
    // 客户端发送数据
    extern void *client_send_action(void *args);
    // 客户端登录
    extern void client_login();
    // 客户端单聊准备
    extern void client_single_setout();
    // 客户端群聊
    extern void client_group_chat();
    // 客户端退出
    extern void client_quit();
    // 客户端群发数据
    extern void client_group_send(char *text);
    // 客户端发送单聊数据
    extern void client_single_send(char *text, int target_fd);
    // 客户端接收群聊数据
    extern void client_group_recv(message *recv_msg);
    // 客户端接收单聊数据
    extern void client_single_recv(message *recv_msg);
    // 客户端单聊线程
    extern int single_chat_socket();
    // 获取当前时间
    extern void get_time_str(char *name);
    // 判断指定客户端名称是否存在顺序表中,存在返回在顺序表中的位置,不存在返回-1
    extern int client_name_is_exist(char *name);

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif // TCP_CLIENT_H