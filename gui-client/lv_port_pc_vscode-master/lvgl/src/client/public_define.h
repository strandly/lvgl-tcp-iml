#ifndef PUBLIC_DEFINE_H
#define PUBLIC_DEFINE_H

// 消息类型
#define LOGIN 'L'   // 登录
#define SINGLE 'S'  // 单聊
#define GROUP 'G'   // 群聊
#define QUIT 'Q'    // 登出
#define AMOUNT 'A'  // 客户端数量
#define CLIENTS 'C' // 所有客户端数据
#define EXIT 'E'    // 退出

// 客户端结构体
typedef struct
{
    int client_fd;                      // 客户端套接字文件描述符
    char name[20];                      // 客户端名称
    char send_head_portrait_name[10];   // 头像名称
    // char ip[20];   // 客户端IP
    // int port;      // 客户端端口地址
} client_info;

// 消息结构体
typedef struct
{
    char msg_type;                      // 消息类型
    // int data_type;                   // 数据类型
    char name[20];                      // 发送消息的客户端的名称
    char send_head_portrait_name[10];   // 头像名称
    int source_fd;                      // 发送消息的文件描述符
    int target_fd;                      // 发送目标,群聊为空,单聊为目标在服务端的文件描述符
    char text[512];                     // 消息正文
} message;

// 顺序表
typedef struct
{
    int capacity; // 顺序表容量
    int size;     // 数组大小
    void **data;  // 顺序存储指针数组
} arr_list;

// client数据
typedef struct
{
    int fd;            // 需要文件描述符是为了防止名称重复
    char name[20];     // 名称
    arr_list *dm_list; // 用于存储该客户端与当前客户端的对话消息
} client_data;

// 对话消息
typedef struct
{
    char sender[20];                    // 发送方
    char send_head_portrait_name[10];   // 发送方的头像名称
    char receiver[20];                  // 接受方
    char text[512];                     // 消息正文
} dialog_msg;

// 群聊对话消息
typedef struct
{
    char sender[20];   // 发送方
    char text[512];    // 消息正文
} group_dialog_msg;

#endif // PUBLIC_DEFINE_H