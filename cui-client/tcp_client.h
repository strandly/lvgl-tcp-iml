#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include <stdio.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <pthread.h>
#include "public_define.h"

#define SERVER_PORT 8000            // 服务器端口
#define SERVER_IP "139.224.247.174" // 服务端公网IP

extern client_info client;  // 客户端数据
extern int flag;  // 用于控制两个线程
extern int read_flag;  // 用于控制读线程是否睡眠,1睡眠
extern pthread_mutex_t mutex;  // 线程互斥锁
extern pthread_cond_t  cond;   // 线程条件变量

// 头像图片名称
    char img_name_arr[10][10] = {"1.png", "2.png", "3.png", "4.png", "5.png",
                                 "6.png", "7.png", "8.png", "9.png", "10.png"};
								 
// socket初始化
int init_socket();
// 客户端接收数据
void *client_recv_action(void *args);
// 客户端发送数据
void *client_send_action(void *args);
// 客户端登录
void client_login();
// 客户端单聊准备
void client_single_setout();
// 客户端单聊线程
int single_chat_socket();
// 客户端单聊
void client_single_chat();
// 客户端群聊
void client_group_chat();
// 客户端退出
void client_quit();
// 客户端群发数据
void client_group_send();

#endif // TCP_CLIENT_H