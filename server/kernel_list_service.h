﻿#ifndef KERNEL_LIST_SERVICE_H
#define KERNEL_LIST_SERVICE_H

/**
内核链表服务
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>
#include "kernel_list.h"
#include "public_define.h"

// 客户端信息
typedef struct {
    char name[20]; // 客户端名称
    int client_fd; // 客户端套接字文件描述符
    char ip[20];   // IP地址
    int port;      // 端口号
    struct list_head list;
} client_node;

// 初始化客户端节点
client_node *init_client_node();

// 获取一个客户端节点
client_node *get_client_node(char *name, int client_fd, char *ip, int port);

// 尾插一个客户端节点
int insert_client(client_node *client_head, client_node *new_client);

// 释放客户端链表所有内存,头节点除外
void destroy_client(client_node *client_head);

// 打印所有客户端链表
void display_all_client(client_node *client_head);

// 打印一个客户端节点
void display_one_client(client_node *client);

// 根据名称查找客户端节点
client_node *search_client_by_name(client_node *client_head, char *name);

// 根据文件描述符查找客户端节点
client_node *search_client_by_fd(client_node *client_head, int client_fd);

// 根据客户端文件描述符删除节点,没找到就不删,成功返回0,失败返回-1
int delete_client(client_node *client_head, int client_fd);

// 获取链表大小
int get_client_count(client_node *client_head);

#endif // KERNEL_LIST_SERVICE_H
