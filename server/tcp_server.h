#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <stdio.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <pthread.h>
#include "public_define.h"
#include "kernel_list_service.h"

#define SERVER_PORT 8000 // 服务器端口
#define LIMIT_CONNECT 20 // 连接的客户端上限

// extern int server_fd;                           // 服务端文件描述符
extern client_node client;                      // 客户端数据
extern client_node client_arr[LIMIT_CONNECT];   // 用于存放连接的客户端信息
extern int client_num;                          // 连接的客户端数量
extern pthread_mutex_t mutex;                   // 初始化线程互斥锁
extern client_node *client_head;                // 客户端链表表头

// socket初始化
int init_socket();
// 监控客户端连接
void *client_connect_action(void *args);
// 服务器接收数据
void *server_recv_action(void *args);
// 服务器发送数据
void *server_send_action(void *args);
// 服务器群发数据
void server_broadcast_msg(client_node *client, message *msg);
// 服务器单发数据
void server_single_msg(client_node *client, message *msg);
// 客户端登录
void client_login(client_node *client, message *msg);
// 客户端单聊
void client_single(client_node *client, message *msg);
// 客户端群聊
void client_group_chat(client_node *client, message *msg);
// 客户端退出
void client_quit(client_node *client, message *msg);
// 客户端获取全部客户端数量
void client_get_client_amount(client_node *client, message *msg);
// 客户端获取全部客户端数据
void client_get_clients(client_node *client, message *msg);
// int转字符串
char *int_to_string(int num, char *str);

#endif  // TCP_SERVER_H